
import java.io.FileInputStream;
import java.io.InputStreamReader;

public class ScannerMain
{

    public static void main( String[] args) 
    {
        String filename = args[0];
        FileInputStream fis = null;
        try {
            fis = new FileInputStream( filename);
        } catch (Exception e ) { e.printStackTrace();}
        InputStreamReader isr = new InputStreamReader( fis);
        ScannerJFlex scanner = new ScannerJFlex( isr);
        
        Token lex = null;
        
        do
        {
        	try {
        			lex = scanner.nextToken();
        		}
        		catch(Exception e) {e.printStackTrace();}
        		
        		
        } while (lex != null);
    }
        		      
     


}